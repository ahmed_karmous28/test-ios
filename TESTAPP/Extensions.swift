//
//  WebServiceManager.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 5/3/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//


import Foundation
import UIKit
import QuartzCore

typealias CompletionBlock = ( _ image:UIImage) -> ()
let loader = UIActivityIndicatorView()


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

extension UIButton {
    
    func setLocalizedString(_ localizedString : String) -> () {
        self.setTitle(NSLocalizedString(localizedString, comment: ""), for: UIControlState())
    }
  
    func downloadedFrom(url: URL) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.setBackgroundImage(image, for: UIControlState.normal)

            }
            }.resume()
        }
    func downloadedFromlink(link: String) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url)
    }
    
    func setImageFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.setImage(image, for: UIControlState.normal)
                
            }
            }.resume()
    }

}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1, height: 1)
        layer.shadowRadius = 1
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}

extension UILabel {
    func setLocalizedString(_ localizedString : String) -> () {
        self.text = NSLocalizedString(localizedString, comment: "")
    }
    
    func convertPointToCommas() {
        self.text = self.text?.replacingOccurrences(of: ".", with: ",")
    }
}

extension UIImageView {
    
    func downloadedFrom(url: URL) {
        downloadedFromUrl(url: url, completion: nil)
        
    }
    
    func downloadedFrom(link: String) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url)
    }
    
    func downloadedFromUrl(url:URL ,completion :  CompletionBlock?) {
        
        loader.center = self.center
        self.addSubview(loader)
        self.bringSubview(toFront: loader)
        loader.startAnimating()

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
                loader.stopAnimating()
                completion?(image)
            }
            }.resume()

    }
}


