//
//  Utilities.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/3/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

var activityIndicatorView: NVActivityIndicatorView!
let backgroundView = UIView()
var isShown = false

class Utilities: NSObject {
    
    struct Singleton {
        static let instance = Utilities()
    }
    
    class var sharedInstance: Utilities {
        return Singleton.instance
    }
    
    static func showLoader(withBackground :Bool = true) {
        //init background view
        if (!isShown){
            isShown = true
            if withBackground {
                backgroundView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
                backgroundView.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.6)
                APP_DELEGATE.navigationController.view.addSubview(backgroundView)
                APP_DELEGATE.navigationController.view.bringSubview(toFront: backgroundView)
            }
            
            let frame = CGRect(x: (SCREEN_WIDTH / 2) - 35, y: (SCREEN_HEIGHT / 2) - 35, width: 100, height: 100)
            let color = UIColor(red: 30 / 255, green: 215 / 255, blue: 96 / 255, alpha: 1)
            activityIndicatorView = NVActivityIndicatorView(frame: frame, type: .ballClipRotatePulse , color: color)
            APP_DELEGATE.navigationController.view.addSubview(activityIndicatorView)
            APP_DELEGATE.navigationController.view.bringSubview(toFront: activityIndicatorView)
            activityIndicatorView.startAnimating()
        }
    }
    
    static func hideLoader() {
        if (isShown){
            isShown = false
            activityIndicatorView.stopAnimating()
            activityIndicatorView.removeFromSuperview()
            backgroundView.removeFromSuperview()
        }
    }
    
}
