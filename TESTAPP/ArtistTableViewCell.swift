//
//  ArtistTableViewCell.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/5/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit

class ArtistTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var artistFollowersLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    func setupCellWithArtist(artist : [String : AnyObject])  {
        bgView.dropShadow()
        artistNameLabel.text = artist["name"]?.description
        let follower : [String : AnyObject] = artist["followers"] as! [String : AnyObject]
        artistFollowersLabel.text = follower["total"]?.description
        let images : [[String : AnyObject]] = artist["images"] as! [[String : AnyObject]]
        if images.count > 0 {
            let imageObject = images[1]
            
            if let artistImageUrl = URL(string: (imageObject["url"]?.description)!) {
                artistImageView.downloadedFromUrl(url: artistImageUrl, completion: nil)
            }
        }
    }
}
