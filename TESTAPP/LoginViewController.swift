//
//  LoginViewController.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/4/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    var loginUrl: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        NotificationCenter.default.addObserver(self, selector: #selector(updateAfterFirstLogin), name: NSNotification.Name(rawValue: "loginSuccessfull"), object: nil)
    }
    
    func setup () {
        auth.redirectURL     = URL(string: redirectURL)
        auth.clientID        = CLIENT_ID
        loginUrl             = auth.spotifyWebAuthenticationURL()
    }
    
    @objc func updateAfterFirstLogin () {
        let userDefaults = UserDefaults.standard
        if let sessionObj:AnyObject = userDefaults.object(forKey: SPOTIFY_SESSION) as AnyObject? {
            let sessionDataObj = sessionObj as! Data
            let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
            self.session = firstTimeSession
            
            //user connected
            UserDefaults.standard.set(true, forKey: CONNECTED)
            UserDefaults.standard.set(self.session.accessToken, forKey: API_TOKEN)
            UserDefaults.standard.set(self.session.encryptedRefreshToken, forKey: REFRESH_TOKEN)
            UserDefaults.standard.synchronize()
            
            let viewController = HomeViewController (nibName: "HomeViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func loginUserToSpotify(_ sender: Any) {
        if UIApplication.shared.openURL(loginUrl!) {
            if auth.canHandle(auth.redirectURL) {
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
