
import Foundation
import UIKit

let APP_NAME = "TESTAPP"

let VERSION_NUMBER = "1.0"
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
let TIMEOUT_WS: TimeInterval = 30
let SPLASH_MININUM_TIME: TimeInterval = 1
let ANIMATION_TIME: TimeInterval = 5

//devices size
let IS_IPHONE_4 =  (UIScreen.main.bounds.size.height < 568)
let IS_IPHONE_5 = (UIScreen.main.bounds.size.height == 568)
let IS_IPHONE_6 = (UIScreen.main.bounds.size.height == 667)
let IS_IPHONE_6_PLUS = (UIScreen.main.bounds.size.height == 736)
let IS_IPHONE_X = (UIScreen.main.bounds.size.height == 812)

//Size screen
let SCREEN_WIDTH    = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT   = UIScreen.main.bounds.size.height

let API_TOKEN       = "api_token"
let REFRESH_TOKEN   = "refresh_token"
let CONNECTED       = "connected"

/* WS */

let BASE_URL            = "https://api.spotify.com/v1/"

/* APP */

let SPOTIFY_SESSION     = "SpotifySession"
let redirectURL         = "TESTAPP://returnAfterLogin"
let CLIENT_ID            = "de7ef7664fac4775ac979ba4b9047205" 

