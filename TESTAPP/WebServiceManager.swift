//
//  WebServiceManager.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/3/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import RMessage


class WebServiceManager: NSObject {
    
    
    struct Singleton {
        static let instance = WebServiceManager()
    }
    
    class var sharedInstance: WebServiceManager {
        return Singleton.instance
    }
    
    static func cancelAllRequests() {
        print("cancelling NetworkHelper requests")
    }
    
    static func callGetSpotifyAPI(url : String, completion: @escaping ((_ response: JSON) -> ()), error: @escaping ((_ error: Error) -> ())) {
        Utilities.showLoader()
        let apiToken = UserDefaults.standard.string(forKey: API_TOKEN)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody  , headers: [ "Authorization": "Bearer \(apiToken!)"]).responseJSON { (responseData) -> Void in

            if let data = responseData.data {
                let json = String(data: data, encoding: String.Encoding.utf8)
                print("Failure Response: \(json ?? "error empty")")
            }

            if(responseData.response?.statusCode == 200) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                print(swiftyJsonVar)
                Utilities.hideLoader()
                completion(swiftyJsonVar)
            } else {
                Utilities.hideLoader()
                guard responseData.error != nil else {
                    RMessage.showNotification(withTitle: NSLocalizedString("error_server", comment: ""), type: .error, customTypeName: nil, callback: nil)
                    return
                }
                error(responseData.error!)
            }
        }
    }
    
    

  
}
