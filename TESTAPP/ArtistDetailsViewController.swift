//
//  ArtistDetailsViewController.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/6/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit

class ArtistDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var topTrack        = [[String : AnyObject]]()
    var relatedArtist   = [[String : AnyObject]]()
    var idArtist        = String()
    var artist          = [String : AnyObject]()
    
    enum TableSection: Int {
        case topTrack = 0, relatedArtist
    }
    
    let SectionHeaderHeight: CGFloat = 30
    var data = [TableSection: [[String: String]]]()
    
    @IBOutlet weak var detailsArtistTableView: UITableView!
    @IBOutlet weak var artistImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         detailsArtistTableView.register(UINib(nibName:"ArtistTableViewCell", bundle: nil), forCellReuseIdentifier: "ArtistTableViewCell")
         detailsArtistTableView.register(UINib(nibName:"TrackTableViewCell", bundle: nil), forCellReuseIdentifier: "TrackTableViewCell")
        
        let images : [[String : AnyObject]] = artist["images"] as! [[String : AnyObject]]
        
        if images.count > 0 {
            let imageObject = images[0]
            
            if let artistImageUrl = URL(string: (imageObject["url"]?.description)!) {
                artistImageView.downloadedFromUrl(url: artistImageUrl, completion: nil)
            }
        }
        
        idArtist = (artist["id"]?.description)!
        getTopTracks()
        
    }
    
    func getRelatesArtists()  {
        let url = BASE_URL + "artists/\(idArtist)/related-artists"
        WebServiceManager.callGetSpotifyAPI(url: url, completion: { (response) in
            print(response)
            self.relatedArtist = response["artists"].arrayObject as! [[String : AnyObject]]
            self.detailsArtistTableView.reloadData()
            
        }) { (error) in
            print(error)
        }
    }
    
    func getTopTracks()  {
        let url = BASE_URL + "artists/\(idArtist)/top-tracks?country=US"
        
        WebServiceManager.callGetSpotifyAPI(url: url, completion: { (response) in
            print(response)
            self.topTrack = response["tracks"].arrayObject as! [[String : AnyObject]]
            self.detailsArtistTableView.reloadData()
            
            self.getRelatesArtists()
        }) { (error) in
            print(error)
            self.getRelatesArtists()
        }
    }
    
    // MARK: tableView Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return topTrack.count
        }else{
            return relatedArtist.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: SectionHeaderHeight))
        view.backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: SectionHeaderHeight))
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor.black
        
        if let tableSection = TableSection(rawValue: section) {
            switch tableSection {
            case .topTrack:
                label.text = NSLocalizedString("top_track", comment: "")
                break
            case .relatedArtist:
                label.text = NSLocalizedString("related_artists", comment: "")
                break
            }
        }
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch (indexPath.section) {
        case 0:
            print("Top track")
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrackTableViewCell") as! TrackTableViewCell
            let track = topTrack[indexPath.row]
            cell.setupTrackCell(track: track)
            return cell
        case 1:
            print("Related artists")
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell") as! ArtistTableViewCell
            let artist = relatedArtist[indexPath.row]
            cell.setupCellWithArtist(artist: artist)
            return cell
        default:
            break
        }

        return UITableViewCell()
    }

    // MARK: tableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section) {
        case 0:
           return 80
        case 1:
            return 100
        default:
            break
        }
        return 100
    }

    
    @IBAction func returnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
