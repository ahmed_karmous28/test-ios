//
//  TrackTableViewCell.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/6/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumtitleLabel: UILabel!
    @IBOutlet weak var songTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTrackCell(track : [String : AnyObject])  {
        bgView.dropShadow() 
        let album : [String : AnyObject] = track["album"] as! [String : AnyObject]
        albumtitleLabel.text = album["name"]?.description

        songTitleLabel.text  = track["name"]?.description
        
        let images : [[String : AnyObject]] = album["images"] as! [[String : AnyObject]]
        if images.count > 0 {
            let imageObject = images[1]
            
            if let artistImageUrl = URL(string: (imageObject["url"]?.description)!) {
                albumImageView.downloadedFromUrl(url: artistImageUrl, completion: nil)
            }
        }
    }
    
}
