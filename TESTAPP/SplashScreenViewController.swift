//
//  SplashScreenViewController.swift
//  Pop Out
//
//  Created by Ahmed Karmous on 2/15/18.
//  Copyright © 2018 Dormalcorp. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var splashBackgroundImage: UIImageView!
    var auth = SPTAuth.defaultInstance()!
    var session:SPTSession!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        splashBackgroundImage.image = getImage()
        Timer.scheduledTimer(timeInterval: SPLASH_MININUM_TIME, target: self, selector: #selector(self.loadingNextview), userInfo: nil, repeats: false);
        UIApplication.shared.isStatusBarHidden = true
    }
    
    @objc func loadingNextview() -> Void {
        
        let connected = UserDefaults.standard.bool(forKey: CONNECTED)
        if connected {

            let userDefaults = UserDefaults.standard
            if let sessionObj:AnyObject = userDefaults.object(forKey: SPOTIFY_SESSION) as AnyObject? {
                let sessionDataObj = sessionObj as! Data
                let firstTimeSession = NSKeyedUnarchiver.unarchiveObject(with: sessionDataObj) as! SPTSession
                auth.tokenRefreshURL = auth.tokenRefreshURL
                auth.tokenSwapURL    = auth.tokenSwapURL
                auth.renewSession(firstTimeSession, callback: { (error, newSession) in
                    if error != nil {
                        print(error as Any)
                    }else{
                        print(newSession as Any);
                    }
                })
            }
            
            let viewController = HomeViewController (nibName: "HomeViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else {
            let viewController = LoginViewController (nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func getImage() -> UIImage {
        
        let parameters : [String: String] = [ "320.0x480.0" : "LaunchImage-700",
                                              "320.0x568.0" : "LaunchImage-700-568h",
                                              "375.0x667.0" : "LaunchImage-800-667h",
                                              "414.0x736.0" : "LaunchImage-800-Portrait-736h",
                                              "375.0x812.0" : "iphone_x",
                                              "768.0x1024.0" : "ipad_port",
        ]
        
        let imageString = "\(SCREEN_WIDTH)x\(SCREEN_HEIGHT)"
        let image = UIImage(named:parameters[imageString]!)
        return image!;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

