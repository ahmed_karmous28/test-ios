    //
//  HomeViewController.swift
//  TESTAPP
//
//  Created by Ahmed Karmous on 4/4/18.
//  Copyright © 2018 Ahmed Karmous. All rights reserved.
//

import UIKit
import SwiftyJSON
import RMessage

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var artistSearchTableView: UITableView!
    @IBOutlet weak var searchTextFIeld: UITextField!
    var searchArray = [[String : AnyObject]]()
    var limit  = 20
    var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        artistSearchTableView.register(UINib(nibName:"ArtistTableViewCell", bundle: nil), forCellReuseIdentifier: "ArtistTableViewCell")
        UIApplication.shared.isStatusBarHidden = false
        artistSearchTableView.addInfiniteScrolling {
            self.page += 1
            self.searchArtist(page: self.page)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    // MARK: tableView Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell") as! ArtistTableViewCell
        let artist = searchArray[indexPath.row]
        cell.setupCellWithArtist(artist: artist)
        return cell
    }
    
    // MARK: tableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let artist = searchArray[indexPath.row]
        let viewController = ArtistDetailsViewController (nibName: "ArtistDetailsViewController", bundle: nil)
        viewController.artist   = artist
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func searchArtist(page : Int)  {
        guard searchTextFIeld.text != "" else {
             RMessage.showNotification(withTitle: NSLocalizedString("empty_search", comment: ""), type: .error, customTypeName: nil, callback: nil)
            return
        }
        print(page)
        let encodedSearch = searchTextFIeld.text?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        let url = BASE_URL + "search?q=\(encodedSearch!)&type=artist&limit=\(limit)&offset=\(page)"

        WebServiceManager.callGetSpotifyAPI(url: url, completion: { (response) in
            print(response)
            let artists = response["artists"].dictionaryObject
            let results = artists!["items"] as! [[String : AnyObject]]
            
            if results.count > 0 {
                self.searchArray.append(contentsOf: results)
                self.artistSearchTableView.reloadData()
            }
            
            self.artistSearchTableView.infiniteScrollingView.stopAnimating()
        }) { (error) in
            print(error)
            self.artistSearchTableView.infiniteScrollingView.stopAnimating()

        }
    }
    
    func getSeveralArtists()  {
        let url = BASE_URL + "artists"
        
        WebServiceManager.callGetSpotifyAPI(url: url, completion: { (response) in
            print(response)
            
            let artists = response["artists"].dictionaryObject
            let results = artists!["items"] as! [[String : AnyObject]]
            
            if results.count > 0 {
                self.searchArray.append(contentsOf: results)
                self.artistSearchTableView.reloadData()
            }
            
            self.artistSearchTableView.infiniteScrollingView.stopAnimating()
        }) { (error) in
            print(error)
            self.artistSearchTableView.infiniteScrollingView.stopAnimating()
            
        }
    }
    
    // MARK: - UITextFieldDelegate

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchArtist(page: page)
        self.view.endEditing(true)
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
